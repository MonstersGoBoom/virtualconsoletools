#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

class Tokenized
{
private:
	unsigned int tok;
public:
	std::vector<std::string> strings;
	//	has a token left 
	bool hasTok();
	//	pull the token as a string
	std::string sTok();
	//	get an int token
	int iTok();

	//	get an float token
	float fTok();
	//	tokenize a file, using ignorepat as seperators 
	Tokenized(std::string filename, std::string ignorepat,bool load = true);
	~Tokenized()
	{
		strings.clear();
	}
};