#include "tool.h"

	//	has a token left 
bool Tokenized::hasTok()
{
	return (tok < strings.size());
}

	//	pull the token as a string
std::string Tokenized::sTok()
{
	if (tok<strings.size())
		return (strings[tok++]);

	return "";
}

//	get an int token
int Tokenized::iTok()
{
	std::string tok;
	tok = sTok();
	return (atol(tok.c_str()));
}

	//	get an float token
float Tokenized::fTok()
{
	std::string tok;
	tok = sTok();
	return (float)(atof(tok.c_str()));
}


	//	tokenize a file, using ignorepat as seperators 

Tokenized::Tokenized(std::string filename, std::string ignorepat,bool load)
{
	FILE *fp;
	//	open the file and barf out asap if fail 
	tok = 0;
	int index = 0;
	unsigned char stringthing[8192];
	memset(stringthing, 0, 8192);
	if (load == true)
	{
		fp = fopen(filename.c_str(), "rb");
		if (fp == NULL)
		{
			printf("file %s error", filename.c_str());
			exit(-1);
		}
		//	we have a valid file , read away 
		while (!feof(fp))
		{
			unsigned char v;
			bool eos = false;
			//	read the byte 
			v = fgetc(fp);
			//	eos is seperator true/false 
			eos = false;
			for (unsigned int x = 0; x < ignorepat.length(); x++)
				if (v == ignorepat.c_str()[x])
					eos = true;
			//	if we hit eos , then whatever is in our buffer is a string to go into the tokens list
			if (eos == true)
			{
				if (index > 0)	//	as long as it's big enough 
				{
					std::string str;
					str.assign((const char*)&stringthing[0], index+1);
					strings.push_back(str);
					memset(stringthing, 0, index + 1);
					index = 0;
				}
			}
			else
			{
				stringthing[index++] = v;
			}
		}
	}
	else
	{
		for (int x = 0 ; x < filename.length() ; x++)
		{
			unsigned char v;
			bool eos = false;
			//	read the byte 
			v = filename[x];
			//	eos is seperator true/false 
			eos = false;
			for (unsigned int x = 0; x < ignorepat.length(); x++)
				if (v == ignorepat.c_str()[x])
					eos = true;
			//	if we hit eos , then whatever is in our buffer is a string to go into the tokens list
			if (eos == true)
			{
				if (index > 0)	//	as long as it's big enough 
				{
					std::string str;
					str.assign((const char*)&stringthing[0], index+1);
					strings.push_back(str);
					memset(stringthing, 0, index + 1);
					index = 0;
				}
			}
			else
			{
				stringthing[index++] = v;
			}
		}
	}
}
