#include "tool.h"
//1 byte - chunk type(tiles - 1, sprites - 2, cover - 3, map - 4, code - 5, sound - 6, music - 7)
//3 bytes - chunk size

void TIC80::ReplaceMap(std::string filename, MAP *tilemap)
{
	FILE *fp;
	int z = 0;
	printf("Writing %s\n", filename.c_str());
	unsigned char *mbuffer = chunks[Map].buffer;
	for (int y = 0; y < 136; y++)
	{
		for (int x = 0; x < 240; x++)
		{
			mbuffer[z++] = tilemap->cell(x, y);
		}
	}

	fp = fopen(filename.c_str(), "wb");
	for (int x = 0; x < Max; x++)
	{
		if (chunks[x].buffer != NULL)
		{
			fwrite(&chunks[x].header, sizeof(TIC_disk_chunk), 1, fp);
			fwrite(chunks[x].buffer, chunks[x].header.size, 1, fp);
		}
	}
	fclose(fp);
}

TIC80::TIC80(std::string filename)
{
	FILE *fp;

	printf("Processing %s\n", filename.c_str());
	for (int x = 0; x < Max; x++)
	{
		chunks[x].buffer = NULL;
		chunks[x].header.size = 0;
		chunks[x].header.type = None;
	}

	fp = fopen(filename.c_str(), "rb");	
	while (!feof(fp))
	{
		TIC_disk_chunk header;
		fread(&header, sizeof(TIC_disk_chunk), 1, fp);
		if (header.type < Max)
		{
			if (header.type == Map)
				header.size = 240 * 136;
			if (header.size != 0)
			{
				chunks[header.type].buffer = new unsigned char[header.size + 1];
				memset(chunks[header.type].buffer, 0, header.size + 1);
				fread(chunks[header.type].buffer, header.size, 1, fp);
			}
			else
				chunks[header.type].buffer = NULL;
			chunks[header.type].header.type = header.type;
			chunks[header.type].header.size = header.size;
		}
		else
		{
			break;
		}
	}
	fclose(fp);
}



TIC80::~TIC80()
{
	for (int x = 0; x < Max; x++)
	{
		if (chunks[x].buffer != NULL)
			delete (chunks[x].buffer);
	}
}

