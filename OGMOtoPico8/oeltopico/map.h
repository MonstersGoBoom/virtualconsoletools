#pragma once
class MAP
{
public:
	bool processTMX(std::string filename);
	bool processOGMO(std::string filename);
	bool processTXM(std::string filename);
	bool processPX(std::string filename);
	void Write(std::string filename, int w, int h, bool binary = true)
	{
		FILE *fp;
		if (binary == true)
		{
			fp = fopen( filename.c_str(), "wb");
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					unsigned char b = cell(x, y);;
					fwrite(&b, 1, 1, fp);
				}
			}
		}
		fclose(fp);
	}

	unsigned char cell(int x, int y);
	~MAP()
	{
		if (buffer != NULL)
		{
			delete buffer;
			buffer = NULL;
		}
	}
	int width, height;
private:
	unsigned char *buffer;
};